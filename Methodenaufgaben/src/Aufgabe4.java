public class Aufgabe4 {
	
	public static final double PI = 3.14159265359;
	
	public static void main(String[] args) {
		System.out.println(quader(2.0, 4.0, 3.0));
		System.out.println(wuerfel(3.0));
		System.out.println(pyramide(5.0, 2.2));
		System.out.println(kugel(2.5));
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double wuerfel(double a) {
		return a * a * a;
	}
	
	public static double pyramide(double a, double h) {
		return a * a * h / 3;
	}
	
	public static double kugel(double r) {
		return 4/3 * r * r * r * PI;
	}
}
