import java.util.Scanner;
public class Fallunterscheidungen_5_OhmschesGesetz {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		
		System.out.println("Welchen Wert wollen Sie berechnen? [R, U, I]:");
		char groesse = my_scanner.next().charAt(0);
		
		double R, U, I;
		
		switch (groesse) {
		case 'R': 
			System.out.println("Bitte geben Sie die Spannung U in Volt an:");
			U = my_scanner.nextDouble();
			System.out.println("Bitte geben Sie die Stromstaerke I in Ampere an:");
			I = my_scanner.nextDouble();
			R = U / I;
			System.out.printf("Der Widerstand R betr�gt %.2f Ohm.", R);
			break;
		case 'U': 
			System.out.println("Bitte geben Sie den Widerstand R in Ohm an:");
			R = my_scanner.nextDouble();
			System.out.println("Bitte geben Sie die Stromstaerke I in Ampere an:");
			I = my_scanner.nextDouble();
			U = R * I;
			System.out.printf("Die Spannung U betr�gt %.2f Volt.", U);
			break;
		case 'I': 
			System.out.println("Bitte geben Sie die Spannung U in Volt an:");
			U = my_scanner.nextDouble();
			System.out.println("Bitte geben Sie den Widerstand R in Ohm an:");
			R = my_scanner.nextDouble();
			I = U / R;
			System.out.printf("Die Stromstaerke I betr�gt %.2f Ampere.", I);
			break;
		default: 
			System.out.println("Sie haben keine g�ltige der drei physikalischen Groessen angegeben."); 
			break;
		}
		
		my_scanner.close();
	}
}
