import java.util.Scanner;
public class Auswahlstrukturen_8_Schaltjahr {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ein Jahr nach Christus an:\n");
		int year = my_scanner.nextInt();
		boolean schaltjahr = false;
		
		if (year % 4 == 0) {
			schaltjahr = true;
		}
		if (year % 4 == 0 && year % 100 == 0) {
			schaltjahr = false;
		}
		if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
			schaltjahr = true;
		}
		
		if (schaltjahr) {
			System.out.println("\nDas Jahr " + year + " ist ein Schaltjahr.");
		} else {
			System.out.println("\nDas Jahr " + year + " ist kein Schaltjahr.");
		}
		
		my_scanner.close();
	}
}
